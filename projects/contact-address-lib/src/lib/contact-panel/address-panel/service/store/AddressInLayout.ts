/**
 * @author: Diwakar Mishra (I350117)
 */
/**
 * AddressLayout for various countrt . Based on this layout address will be rendered in UI
 */
export const AddressInLayout = {
    "JE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "GG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "UNKNOWN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "MR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "YT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "YE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "XK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "WF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "VU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "UZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "UM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "TL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TJ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SJ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "RW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "RE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "PW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "PS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "PN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "PM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "PF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "NU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "NR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "NF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "NC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "MP": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "MH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "MG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "KM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "KI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "KG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "IO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "HM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GQ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GP": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "GA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "FO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "FM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "ER": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "EH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "CX": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "ME": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "RS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "CS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "CK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "CG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "CC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "BV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "BJ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressCountry"
            ]
        }
    },
    "BF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "AS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "AQ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "AI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "ZW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "ZM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "YU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "VN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "VE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "VA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "VI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "UY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "US": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GB": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "UA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "UG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "TD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressProvinceText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressProvinceText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "LK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "ES": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "KR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "ZA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressProvince",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressProvince",
                "LOEInAddressCountry"
            ]
        }
    },
    "SO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SB": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "SC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressPostalCode",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "ST": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "WS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "VC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "RU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "RO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "QA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressProvinceText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressProvinceText",
                "LOEInAddressCountry"
            ]
        }
    },
    "PK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "OM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "KP": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NP": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "NA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MX": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressMunicipality",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MQ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "ML": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "MW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "MO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LB": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "LA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "JO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "JP": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressTaxTerritoryPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "JM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IQ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "ID": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "IS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "HU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "HK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "HN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "HT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "GM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "FR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "FI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "FJ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "FK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "ET": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "EE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "SV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "EG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "EC": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DJ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DK": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "HR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    },
    "CO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressMunicipality",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CV": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "CM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "KH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BI": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BN": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "VG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressStreet3",
                "LOEInAddressStreet4",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressMunicipality",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BA": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BE": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BY": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BB": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BS": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BH": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AT": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AU": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AR": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AM": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AW": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AD": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AO": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AG": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AF": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "AL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "DZ": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressStateText",
                "LOEInAddressCountry"
            ]
        }
    },
    "BL": {
        "showCountryStateOnly": {
            field: [
                "LOEInAddressCountry"
            ]
        },
        "default": {
            field: [
                "LOEInAddressStreet1",
                "LOEInAddressStreet2",
                "LOEInAddressPostalCode",
                "LOEInAddressCity",
                "LOEInAddressCountry"
            ]
        }
    }
}
