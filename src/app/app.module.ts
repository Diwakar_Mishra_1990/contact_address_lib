import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ContactAddressLibModule} from 'contact-address-lib'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextSelectBoxComponent } from './text-select-box/text-select-box.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TextSelectBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ContactAddressLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
