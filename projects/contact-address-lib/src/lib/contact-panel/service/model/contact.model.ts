import { WidgetModel } from '../../../widget/model/widget.model';
import { EmailModel } from '../../email-widget/service/model/email.model';
import { PhoneModel } from '../../phone-widget/service/model/phone.model';
import { AddressModel } from '../../address-panel/service/model/address.model';
import { DepartmentModel } from '../model/department.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */

//TODO:Extended WidgetModel . Part of bigger change in framework for widgets
export class ContactModel implements WidgetModel{
    private role: string;
    private name: Name;
    private email: EmailModel[]=[];
    private phone: PhoneModel[]=[];
    private postalAddress: AddressModel[]=[];
    private departmentName: DepartmentModel;
    public getDepartmentName(): DepartmentModel {
        return this.departmentName;
    }
    public setDepartmentName(value: DepartmentModel) {
        this.departmentName = value;
    }
    public setJsonDepartmentName(value: any) {
        let department = new DepartmentModel();
        department.createDepartment(value);  
        this.setDepartmentName(department);
    }
    public getRole(): string {
        return this.role;
    }
    public setRole(value: string) {
        if(value){
            this.role = value;
        }
        else {
            this.role ="";
        }
    }
    public getName(): Name {
        return this.name;
    }
    public setName(value: Name) {
        this.name = value;
    }
    public  setJsonName(value: any) {
        let name = new Name();
        name.createName(value)
        this.setName(name);
    }

    public getEmail(): EmailModel[] {
        return this.email;
    }
    public setJsonEmail(value: any) {
        if(value){
            for(var i =0 ;i<value.length;i++){
                let email = new EmailModel();
                this.getEmail().push(<EmailModel>email.createWidgetModel(value[i]));
            }
        }
        else{
            let email = new EmailModel();
            this.getEmail().push(<EmailModel>email.createWidgetModel(value));
        }
    }
    public setEmail(value: EmailModel[]) {
        this.email = value;
    }
    public getPhone(): PhoneModel[] {
        return this.phone;
    }
    public setJsonPhone(value: any) {
        if(value){
            for(var i =0 ;i<value.length;i++){
                let phone = new PhoneModel()
                this.getPhone().push(phone.createPhone(value[i]));
            }
        }
        else{
            let phone = new PhoneModel()
            this.getPhone().push(phone.createPhone(value));
        }
    }
    public setPhone(value: PhoneModel[]) {
        this.phone = value;
    }
    public getPostalAddress(): AddressModel[] {
        return this.postalAddress;
    }
    public setPostalAddress(value: AddressModel[]) {
        this.postalAddress = value;
    }
    public setJsonPostalAddress(value: any) {
        if(value && Array.isArray(value)){
            for(var i =0 ;i<value.length;i++){
                let paddress = new AddressModel();
                this.getPostalAddress().push(paddress.createPostalAddress(value[i]));
            }
        }else{
            let postalAddress = new AddressModel();
            postalAddress.createPostalAddress(value);  
            this.postalAddress.push(postalAddress);
        }
        
    }

    createWidgetModel(value: any): ContactModel {
        return this.createContactModel(value);
    }
    public  createContactModel(value:any):ContactModel{
        if(value){
            this.setRole(value.role);
            this.setJsonEmail(value.email);
            this.setJsonName(value.name);
            this.setJsonPhone(value.phone);
            this.setJsonPostalAddress(value.postalAddress);
            this.setJsonDepartmentName(value.departmentName);
        }
        else{
            this.setRole(value);
            this.setJsonEmail(value);
            this.setJsonName(value);
            this.setJsonPhone(value);
            this.setJsonPostalAddress(value);
            this.setJsonDepartmentName(value);
        }
        return this;
    }
}

export class Name{
    private locale: string;
    public getLocale(): string {
        return this.locale;
    }
    public setLocale(value: string) {
        if(value){
            this.locale = value;
        }
        else{
            this.locale = "";
        }
    }
    private value: string;
    public getValue(): string {
        return this.value;
    }
    public setValue(value: string) {
        if(value){
            this.value = value;
        }
        else{
            this.value = "";
        }
    }
    public  createName(value:any):Name{
        if(value){
            this.setLocale(value.locale);
            this.setValue(value.value);
        }
        else{
            this.setLocale(value);
            this.setValue(value);
        }
        return this;
    }
}