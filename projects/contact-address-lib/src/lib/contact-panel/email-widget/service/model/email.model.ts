import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Extended WidgetModel . Part of bigger change in framework for widgets
export class EmailModel implements WidgetModel{
    public value: string;
    public name: string; 

    public getValue(): string {
        return this.value;
    }
    public setValue(value: string) {
        if(value){
            this.value = value;
        }else{
            this.value = "";
        }
    }
    public getName(): string {
        return this.name;
    }
    public setName(value: string) {
        if(value){
            this.name = value;
        }else{
            this.name = "";
        }
    }
    
    createWidgetModel(value: any):WidgetModel {
        return this.createEmail(value);
    }
    
    public createEmail(value:any):EmailModel{
        if(value){
            this.setName(value.name);
            this.setValue(value.value);
        }
        else{
            this.setName(value);
            this.setValue(value)
        }
        return this;
    }
}