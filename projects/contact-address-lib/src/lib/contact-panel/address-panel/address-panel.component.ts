import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'address-panel',
  templateUrl: './address-panel.component.html',
  styleUrls: ['./address-panel.component.css']
})
export class AddressPanelComponent implements OnInit, OnChanges {


  @Input() itemSize: number = 0;
  @Input() MAX_SIZE: number = 12;
  @Input() addressLayout: [];
  addressLayoutSequenceList: [][] = [];
  @Input() panelLayout = "hBox"
  displayRows: boolean = false;
  unitItemSize: string;
  unitItemArray: number[]
  constructor() { }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.itemSize >= 1 && this.itemSize <= this.MAX_SIZE) {
      this.unitItemArray = Array(this.itemSize).fill(0).map((x, i) => i);
      let unitSize = (100 / this.itemSize);
      this.unitItemSize = unitSize + "%";
      this.createAddressLayoutForCoulmns();
    }
    if (this.itemSize >= 1) {
      this.displayRows = true;
    }
    else {
      this.displayRows = false;
    }
  }

  createAddressLayoutForCoulmns() {
    this.addressLayoutSequenceList = [];
    let test = [];
    let addressLayoutSequenceIndex = 0;
    for (let i = 0; i < this.itemSize; i++) {
      this.addressLayoutSequenceList.push([]);
    }
    for (var i = 0; i < this.addressLayout.length; i++) {
      this.addressLayoutSequenceList[addressLayoutSequenceIndex].push(this.addressLayout[i]);
      addressLayoutSequenceIndex++;
      if (addressLayoutSequenceIndex == this.itemSize) {
        addressLayoutSequenceIndex = 0;
      }
    }
  }
}

