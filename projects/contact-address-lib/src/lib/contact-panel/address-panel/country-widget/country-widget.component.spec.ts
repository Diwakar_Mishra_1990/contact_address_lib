import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryWidgetComponent } from './country-widget.component';

describe('CountryWidgetComponent', () => {
  let component: CountryWidgetComponent;
  let fixture: ComponentFixture<CountryWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
