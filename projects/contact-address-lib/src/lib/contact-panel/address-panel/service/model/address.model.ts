/**
 * @author:DIWAKAR MISHRA(I350117)
 */

import { WidgetModel } from '../../../../widget/model/widget.model';
import { CountryModel } from '../../country-widget/service/model/country.model';
import { CityModel } from '../../city-widget/service/model/city.model';
import { StateModel } from '../../state-widget/service/model/state.model';
import { MunicipalityModel } from '../../municipality-widget/service/model/municipality.model';

export class AddressModel implements WidgetModel{
    private name: string;
    private street1: string;
    private street2: string;
    private street3: string;
    private street4: string;
    private deliverTo: string;
    private city: any;
    private municipalty: any;
    private state: any;
    private postalCode: string;
    private country: CountryModel;
    public getCountry(): CountryModel {
        return this.country;
    }
    public setJsonCountry(value: any) {
        let country = new CountryModel();
        country.createCountry(value);
        this.setCountry(country);
    }

    public setCountry(value: CountryModel) {
        this.country = value;
    }
    public getName(): string {
        return this.name;
    }
    public setName(value: string) {
        if(value){
            this.name = value;
        }
        else{
            this.name = "";
        }
    }
    public getStreet1(): string {
        return this.street1;
    }
    public setStreet1(value: string) {
        if(value){
            this.street1 = value;
        }
        else{
            this.street1 = "";
        }
    }

    public getStreet2(): string {
        return this.street2;
    }
    public setStreet2(value: string) {
        if(value){
            this.street2 = value;
        }
        else{
            this.street2 = "";
        }
    }
    public getStreet3(): string {
        return this.street3;
    }
    public setStreet3(value: string) {
        if(value){
            this.street3 = value;
        }
        else{
            this.street3 = "";
        }
    }
    public getStreet4(): string {
        return this.street4;
    }
    public setStreet4(value: string) {
        if(value){
            this.street4 = value;
        }
        else{
            this.street4 = "";
        }
    }
    public getCity(): any {
        return this.city;
    }
    public setCity(value: any) {
        this.city = value;
    }
    public setJsonCity(value: any) {
        let city = new CityModel();
        city.createCity(value);
        this.setCity(city);
    }
    public getState(): any {
        return this.state;
    }
    public setState(value: any) {
        this.state = value;
    }
    public setJsonState(value: any) {
        let state = new StateModel();
        state.createState(value);
        this.setState(state);
    }
    public getPostalCode(): string {
        return this.postalCode;
    }
    public setPostalCode(value: string) {
        if(value){
            this.postalCode = value;
        }
        else{
            this.postalCode = "";
        }
        
    }

    public getDeliverTo(): string {
        return this.deliverTo;
    }
    public setDeliverTo(value: string) {
        if(value){
            this.deliverTo = value;
        }
        else{
            this.deliverTo = "";
        }
        
    }

    public getMunicipalty(): any {
        return this.municipalty;
    }
    public setMunicipalty(value: any) {
        this.municipalty = value;
    }

    public setJsonMunicipalty(value: any) {
        let municipalty = new MunicipalityModel();
        municipalty.createMunicipality(value);
        this.setMunicipalty(municipalty);
    }

    public createPostalAddress(value): AddressModel {
        if (value) {
            this.setName(value.name);
            if (value.street) {
                this.setStreet1(value.street[0]);
                this.setStreet2(value.street[1]);
                this.setStreet3(value.street[2]);
                this.setStreet4(value.street[3]);
            }
            else {
                this.setStreet1(value.street1);
                this.setStreet2(value.street2);
                this.setStreet3(value.street3);
                this.setStreet4(value.street4);
            }
            if(value.deliverTo && 
                Array.isArray(value.deliverTo) && 
                value.deliverTo.length>0){
                this.setDeliverTo(value.deliverTo[0]);
            }
            else{
                this.setDeliverTo(value.deliverTo);
            }
            this.setJsonCity(value.city);
            this.setPostalCode(value.postalCode);
            this.setJsonState(value.state);
            this.setJsonCity(value.city);
            this.setJsonMunicipalty(value.municipality);
            this.setJsonCountry(value.country);

        }else{
            this.setName(value);
            this.setStreet1(value);
            this.setStreet2(value);
            this.setStreet3(value);
            this.setStreet4(value);
            this.setPostalCode(value);
            this.setJsonCity(value);
            this.setJsonMunicipalty(value);
            this.setJsonState(value);
            this.setJsonCountry(value);
        }
        return this;
    }

    createWidgetModel(value: any): AddressModel {
        return this.createPostalAddress(value);
    }
}