import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ContactAddressLibComponent } from './contact-address-lib.component';
import { ContactPanelComponent } from './contact-panel/contact-panel.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule, TranslateCompiler } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AddressPanelComponent } from './contact-panel/address-panel/address-panel.component';
import { EmailWidgetComponent } from './contact-panel/email-widget/email-widget.component';
import { PhoneWidgetComponent } from './contact-panel/phone-widget/phone-widget.component';
import { CountryWidgetComponent } from './contact-panel/address-panel/country-widget/country-widget.component';
import { StateWidgetComponent } from './contact-panel/address-panel/state-widget/state-widget.component';
import { CityWidgetComponent } from './contact-panel/address-panel/city-widget/city-widget.component';
import { MunicipalityWidgetComponent } from './contact-panel/address-panel/municipality-widget/municipality-widget.component';
import { TextSelectBoxComponent } from './widget/text-select-box/text-select-box.component';


@NgModule({
  declarations: [
    ContactAddressLibComponent,
    ContactPanelComponent,
    AddressPanelComponent, 
    EmailWidgetComponent, 
    PhoneWidgetComponent, 
    CountryWidgetComponent, 
    StateWidgetComponent, 
    CityWidgetComponent, 
    MunicipalityWidgetComponent, TextSelectBoxComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [ContactAddressLibComponent]
})
export class ContactAddressLibModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}