import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'text-select-box',
  templateUrl: './text-select-box.component.html',
  styleUrls: ['./text-select-box.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})
export class TextSelectBoxComponent implements OnInit {
  @Input() formGroup:FormGroup;
  @Input() selectedText: string = "";
  @Input() selectedModel:any;
  selectedModelIndex:number=0;
  @Input() emptyText =  "Search .." 
  isOpen: Boolean = false;
  @Input()displayString:Function;
  @Input() inputList = [];
  constructor(private _eref: ElementRef) { }
  ngOnInit() {

  }
  filterFunction(event) {
    if (this.selectedText.length == 0) {
      this.isOpen = false;
      this.selectedModelIndex = -1;
    }
    else {
      this.isOpen = true;
    }
  }
  toggleDropDown() {
    this.isOpen = !this.isOpen;
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) {     
      this.isOpen = false;
    }
    else {
      if(this.isOpen && event.target.type !== 'submit'){
        this.selectedText = event.target.textContent;
        this.selectedModelIndex = event.target.name;
        this.isOpen = false;
        if(this.formGroup){
          this.formGroup.patchValue(this.inputList[this.selectedModelIndex]);
        }
      }
      else if(!this.isOpen && event.target.type === 'text'){
        this.isOpen = true;
      }
    }
  }
}


