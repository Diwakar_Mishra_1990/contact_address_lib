import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MunicipalityWidgetComponent } from './municipality-widget.component';

describe('MunicipalityWidgetComponent', () => {
  let component: MunicipalityWidgetComponent;
  let fixture: ComponentFixture<MunicipalityWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MunicipalityWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MunicipalityWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
