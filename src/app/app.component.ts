import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ContactAddressModule';
  
  displayString(item:sampleModel){
    return item.value+ "####"+ item.code;
  }

  inputList = [
    {
      code:"ab",
      value:"About"
    },
    {
      code:"bs",
      value:"Base"
    },
    {
      code:"bg",
      value:"Blog"
    },
    {
      code:"Ct",
      value:"Contact"
    }
    ];
}
export class sampleModel{
  value:string;
  code: string
}