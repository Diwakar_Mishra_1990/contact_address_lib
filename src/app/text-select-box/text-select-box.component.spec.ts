import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextSelectBoxComponent } from './text-select-box.component';

describe('TextSelectBoxComponent', () => {
  let component: TextSelectBoxComponent;
  let fixture: ComponentFixture<TextSelectBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextSelectBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextSelectBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
