import { TestBed } from '@angular/core/testing';

import { ContactPanelService } from './contact-panel.service';

describe('ContactPanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactPanelService = TestBed.get(ContactPanelService);
    expect(service).toBeTruthy();
  });
});
