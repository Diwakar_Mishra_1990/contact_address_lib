import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Extended WidgetModel . Part of bigger change in framework for widgets
export class CountryModel implements WidgetModel{
    
    private value: string;
    private isoCountryCode: string;

    public setValue(value: string) {
        if(value){
            this.value = value;
        }else{
            this.value = "";
        }
    }
    public getValue(): string {
        return this.value;
    }
    public getIsoCountryCode(): string {
        return this.isoCountryCode;
    }
    public setIsoCountryCode(value: string) {
        if(value){
            this.isoCountryCode = value;
        }else{
            this.isoCountryCode = "";
        }
    }
    public  createCountry(value:any) :CountryModel{
        if(value){
            this.setIsoCountryCode(value.isoCountryCode);
            this.setValue(value.value);
        }
        else{
            this.setIsoCountryCode(value);
            this.setValue(value);
        }
        return this;
    }

    createWidgetModel(value: any): CountryModel {
        return this.createCountry(value);
    }
    
}