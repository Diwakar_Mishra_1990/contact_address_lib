import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Extended WidgetModel . Part of bigger change in framework for widgets
export class CityModel implements WidgetModel {

    private value: string;
    private isoCityCode: string;
    private compositeCode: string;

    public getCompositeCode(): string {
        return this.compositeCode;
    }
    public setCompositeCode(value: string) {
        if (value) {
            this.compositeCode = value;
        }
        else {
            this.compositeCode = "";
        }
    }

    public setValue(value: string) {
        if (value) {
            this.value = value;
        }
        else {
            this.value = "";
        }
    }
    public getValue(): string {
        return this.value;
    }
    public getIsoCityCode(): string {
        return this.isoCityCode;
    }
    public setIsoCityCode(value: string) {
        if (value) {
            this.isoCityCode = value;
        }
        else {
            this.isoCityCode = "";
        }
    }

    public createCity(value: any): CityModel {
        if (value) {
            this.setIsoCityCode(value.isoCityCode);
            this.setValue(value.value);
            this.setCompositeCode(value.compositeCode);
        }
        else {
            this.setIsoCityCode(value);
            this.setValue(value);
            this.setCompositeCode(value);
        }
        return this;
    }
    createWidgetModel(value: any): CityModel {
        return this.createCity(value);
    }
}