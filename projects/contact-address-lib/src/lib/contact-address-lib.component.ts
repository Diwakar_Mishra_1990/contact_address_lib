import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  //selector: 'lib-contact-address-lib',
  selector: 'contact-address',
  templateUrl: './contact-address-lib.component.html',
  styles: []
})
export class ContactAddressLibComponent implements OnInit {

  ngOnInit() {
  }
  itemSize: number;
  title = 'ContactAddress';
  addressSequence = [
    "LOEInAddressStreet1",
    "LOEInAddressStreet2",
    "LOEInAddressStreet3",
    "LOEInAddressStreet4",
    "LOEInAddressCity",
    "LOEInAddressStateText",
    "LOEInAddressPostalCode",
    "LOEInAddressCountry"
  ];
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
}
