import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'contact-panel',
  templateUrl: './contact-panel.component.html',
  styleUrls: ['./contact-panel.component.css']
})
export class ContactPanelComponent implements OnInit {

  constructor(private translate: TranslateService) { }
  @Input()itemSize:number;
  @Input()addressLayout:any;

  ngOnInit() {
  }
  languageMenu = ["en","de","ar","en-US","es","fi","hi","it","ja","nl","pt","ru","zh","zu"];
  selectedLanguage(lang:any){
    console.log(lang);
    this.translate.use(lang);
  }
}
