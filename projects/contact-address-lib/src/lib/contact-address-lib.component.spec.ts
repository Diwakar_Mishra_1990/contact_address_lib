import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactAddressLibComponent } from './contact-address-lib.component';

describe('ContactAddressLibComponent', () => {
  let component: ContactAddressLibComponent;
  let fixture: ComponentFixture<ContactAddressLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactAddressLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactAddressLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
