import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Part of a bigger change in Widget framework
export class PhoneModel implements WidgetModel {
   
    private telephoneNumber: TelephoneNumber;
    private name: string;
    public getName(): string {
        return this.name;
    }
    public setName(value: string) {
        this.name = value;
    }
    public getTelephoneNumber(): TelephoneNumber {
        return this.telephoneNumber;
    }
    public setTelephoneNumber(value: TelephoneNumber) {
        this.telephoneNumber = value;
    }
    public setJsonTelephoneNumber(value: any) {
        let telNumber = new TelephoneNumber();
        telNumber.createTelephoneNumber(value)
        this.setTelephoneNumber(telNumber);
    }

    public createPhone(value: any): PhoneModel {
        if (value) {
            this.setJsonTelephoneNumber(value.telephoneNumber);
            this.setName(value.name);
        }
        else {
            this.setJsonTelephoneNumber(value);
            this.setName(value);
        }
        return this;
    }

    createWidgetModel(value: any): WidgetModel {
        return this.createPhone(value);
    }
}

export class TelephoneNumber {
    private countryCode: Country;
    private areaOrCityCode: string;
    private number: string;
    private extension: string;
    public getExtension(): string {
        return this.extension;
    }
    public setExtension(value: string) {
        if(value){
            this.extension = value;
        }
        else{
            this.extension = "";
        }
    }
    public getNumber(): string {
        return this.number;
    }
    public setNumber(value: string) {
        if(value){
            this.number = value;
        }
        else{
            this.number = "";
        }
    }
    public getAreaOrCityCode(): string {
        return this.areaOrCityCode;
    }
    public setAreaOrCityCode(value: string) {
        if(value){
            this.areaOrCityCode = value;
        }
        else{
            this.areaOrCityCode = "";
        }
    }
    public getCountryCode(): Country {
        return this.countryCode;
    }
    public setCountryCode(value: Country) {
        this.countryCode = value;
    }
    public setJsonCountryCode(value: Country) {
        let country = new Country();
        country.createCountry(value);
        this.setCountryCode(country)
    }
    public createTelephoneNumber(value: any): TelephoneNumber {
        if (value) {
            this.setJsonCountryCode(value.countryCode);
            this.setAreaOrCityCode(value.areaOrCityCode);
            this.setExtension(value.extension);
            this.setNumber(value.number);
        } else {
            this.setJsonCountryCode(value);
            this.setAreaOrCityCode(value);
            this.setExtension(value);
            this.setNumber(value);
        }

        return this;
    }
}

export class Country {
    private value: string;
    private isoCountryCode: string;

    public getValue(): string {
        return this.value;
    }
    public setValue(value: string) {
        if(value){
            this.value = value;
        }
        else{
            this.value = "";
        }
    }
    public getIsoCountryCode(): string {
        return this.isoCountryCode;
    }
    public setIsoCountryCode(value: string) {
        if(value){
            this.isoCountryCode = value;
        }else{
            this.isoCountryCode = "";
        }
        
    }

    public createCountry(value: any): Country {
        if (value) {
            this.setValue(value.value);
            this.setIsoCountryCode(value.isoCountryCode);
        }
        else {
            this.setValue(value);
            this.setIsoCountryCode(value);
        }
        return this;
    }
}