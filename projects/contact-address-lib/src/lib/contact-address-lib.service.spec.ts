import { TestBed } from '@angular/core/testing';

import { ContactAddressLibService } from './contact-address-lib.service';

describe('ContactAddressLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactAddressLibService = TestBed.get(ContactAddressLibService);
    expect(service).toBeTruthy();
  });
});
