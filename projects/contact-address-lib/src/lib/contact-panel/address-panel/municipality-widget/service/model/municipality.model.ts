import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Extended WidgetModel . Part of bigger change in framework for widgets
export class MunicipalityModel implements WidgetModel {

    private value: string;
    private isoMunicipalityCode: string;
    private compositeCode: string;

    public getCompositeCode(): string {
        return this.compositeCode;
    }
    public setCompositeCode(value: string) {
        if (value) {
            this.compositeCode = value;
        } else {
            this.compositeCode = "";
        }
    }

    public setValue(value: string) {
        if (value) {
            this.value = value;
        } else {
            this.value = "";
        }
    }
    public getValue(): string {
        return this.value;
    }
    public getIsoMunicipalityCode(): string {
        return this.isoMunicipalityCode;
    }
    public setIsoMunicipaltiyCode(value: string) {
        if(value){
            this.isoMunicipalityCode = value;
        }else{
            this.isoMunicipalityCode = "";
        }
    }

    public createMunicipality(value: any): MunicipalityModel {
        if (value) {
            this.setIsoMunicipaltiyCode(value.isoMunicipaltyCode);
            this.setValue(value.value);
            this.setCompositeCode(value.compositeCode);
        }
        else {
            this.isoMunicipalityCode = "";
            this.value = "";
            this.compositeCode = "";
        }
        return this;
    }
    createWidgetModel(value: any): MunicipalityModel {
        return this.createMunicipality(value);
    }
}