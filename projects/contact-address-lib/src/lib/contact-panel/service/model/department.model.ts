import { WidgetModel } from '../../../widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
export class DepartmentModel implements WidgetModel{
    createWidgetModel(value: any): DepartmentModel {
        return this.createDepartment(value);
    }
    private creator: string;
    public getCreator(): string {
        return this.creator;
    }
    public setCreator(value: string) {
        if(value){
            this.creator = value;
        }else{
            this.creator = "";
        }
        
    }
    private description: string;
    public getDescription(): string {
        return this.description;
    }
    public setDescription(value: string) {
        if(value){
            this.description = value;
        }else{
            this.description = "";
        }
    }
    private identifier: string;
    public getidentifier(): string {
        return this.identifier;
    }
    public setIdentifier(value: string) {
        if(value){
            this.identifier = value;
        }else{
            this.identifier = "";
        }
    }
    private domain: string;
    public getDomain(): string {
        return this.domain;
    }
    public setDomain(value: string) {
        if(value){
            this.domain = value;
        }else{
            this.domain = "";
        }
    }
    public  createDepartment(value:any):DepartmentModel{
        if(value){
            this.setCreator(value.creator);
            this.setDescription(value.description);
            this.setDomain(value.domain);
            this.setIdentifier(value.identifier);
        }else{
            this.setCreator(value);
            this.setDescription(value);
            this.setDomain(value);
            this.setIdentifier(value);
        }
        return this;
    }
}