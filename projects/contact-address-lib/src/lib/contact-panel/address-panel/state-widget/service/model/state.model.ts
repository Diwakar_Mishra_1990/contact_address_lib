import { WidgetModel } from 'projects/contact-address-lib/src/lib/widget/model/widget.model';

/**
 * @author: DIWAKAR MISHRA(I350117)
 */
//TODO:Part of biiger change in Widgert components
export class StateModel implements WidgetModel {
    

    private value: string;
    private isoStateCode: string;
    private compositeCode: string;

    public getCompositeCode(): string {
        return this.compositeCode;
    }
    public setCompositeCode(value: string) {
        if(value){
            this.compositeCode = value;
        }
       else{
           this.compositeCode = "";
       }    
    }

    public setValue(value: string) {
        if(value){
            this.value = value;
        }
       else{
           this.value = "";
       }
    }
    public getValue(): string {
        return this.value;
    }
    public getIsoStateCode(): string {
        return this.isoStateCode;
    }
    public setIsoStateCode(value: string) {
        if(value){
            this.isoStateCode = value;
        }
       else{
           this.isoStateCode = "";
       }
    }

    public createState(value: any): StateModel {
        if (value) {
            this.setIsoStateCode(value.isoStateCode);
            this.setValue(value.value);
            this.setCompositeCode(value.compositeCode);
        }
        else{
            this.setIsoStateCode(value);
            this.setValue(value);
            this.setCompositeCode(value);
        }
        return this;
    }

    createWidgetModel(value: any): StateModel {
        return this.createState(value);
    }

}