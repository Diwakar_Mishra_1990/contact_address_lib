/*
 * Public API Surface of contact-address-lib
 */

export * from './lib/contact-address-lib.service';
export * from './lib/contact-address-lib.component';
export * from './lib/contact-address-lib.module';
