import { OnInit, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormArray, ControlValueAccessor } from '@angular/forms';
import { WidgetModel } from './model/widget.model';

export class WidgetComponent implements OnInit,OnDestroy, OnChanges{
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.group && changes.group !== null){
      if(!changes.group.currentValue || changes.group.currentValue ===null){
        this.group = changes.group.previousValue;
      }
    }
    if(changes.groupArray && changes.groupArray !== null){
      if(!changes.groupArray.currentValue || changes.groupArray.currentValue ===null){
        this.groupArray = changes.groupArray.previousValue;
      }
    }
    if(changes.parent && changes.parent !== null){
      if(!changes.parent.currentValue || changes.parent.currentValue ===null){
        this.parent = changes.parent.previousValue;
      }
    }
  }

  /**
   * Form Array pertaining to WidgetComponent
   */
  @Input() groupArray: FormArray;
  /**
   * Form Group pertaining to WidgetComponent 
   */
  @Input() group: FormGroup;
  /**
   * Input JSON DATA for widget
   */
  @Input() data: any;
  /**
   * Widget model which is associated with component 
   */
  @Input() widgetModel: WidgetModel;
  /**
   * Widget model list which is associated with component in case wiget is a form array
   */
  @Input() widgetModelList: WidgetModel[];
  /**
   * This checks  whether widget should be rendered in READ_ONLY or EDIT mode.
   * This means that by default edit email will be referred in default mode
   */
  @Input('READ_ONLY') readOnly = false;
  /**
   * This value checks whether widget is for a formGroup or a formArray . Default value
   * is represents a widget group . In order to check for formArray we need to set the 
   * value to true
   */
  @Input('IS_WIDGET_ARRAY') isWidgetArray = false;
  /**
   * Parent associated with form group
   */
  @Input() parent: FormGroup;
  /**
   * meant for display
   */
  @Input('COMPACT') compact = false;

  subscriptionList:any=[];
  /**
   * WIDGET GROUP NAME
   */
  @Input('WIDGET_GROUP_NAME')WIDGET_GROUP_NAME:string;

  constructor() {
  }

  ngOnInit(): void {
    if (this.isWidgetArray) {
      //If group array is null then we need to create group array
      if(!this.groupArray || this.groupArray.length==0){
        this.createArrayGroup();
        this.associateArrayGroupToParent();
      }
    }
    else {
      //If group is null , then we need to group
      if (!this.group || Object.keys(this.group.value).length == 0 ) {
        //If group is empty create or initialize group
        this.createGroup();
        this.associateGroupToParent();
      }
    }
    if(this.readOnly){
      this.addSubscriptionForReadMode();
    }
    else{
      this.addSubscriptionForEditMode();
    }
  }

  /*
  =============================================================================================================
  ===================================OPERATIONS RELATED WHEN OUR WIDGET USES A GROUP=======================
  =============================================================================================================
  */
  /**
   * Creates group 
   */
  createGroup() {
    if (this.widgetModel) {
      //Initialize from Widget Model
      this.initializeGroupFromWidgetModel();
    }
    else if (this.data) {
      //Initialize from data
      this.initializeGroupFromData();
    } else {
      //Initialize empty data
      this.initializeEmptyFormGroup();
    }
  }
  initializeGroupFromWidgetModel() { }
  initializeGroupFromData() { }
  initializeEmptyFormGroup() {
    if(this.widgetModel){
      this.widgetModel.createWidgetModel(null);
    }
  }
  createGroupFromWidgetModel(widgetModel:WidgetModel){}

  associateGroupToParent() {
      if(this.parent){
        this.parent.setControl(this.WIDGET_GROUP_NAME,this.group);
      }
  }
  /*
  =============================================================================================================
  ===================================OPERATIONS RELATED WHEN OUR WIDGET IS A FORMARRAY=======================
  =============================================================================================================
  */
  /**
   * Creates Array group
   */
  createArrayGroup() {
    if (this.widgetModelList && this.widgetModelList.length > 0) {
      //Initialize from Widget Model
      this.initializeGroupArrayFromWidgetModel();
    }
    else if (this.data && this.data.length>0) {
      //Initialize from data
      this.widgetModelList=[];
      this.initializeGroupArrayFromData();
    } else {
      //Initialize empty form group
      this.widgetModelList=[];
      this.initializeEmptyFormArray();
    }
  }
  
  initializeGroupArrayFromWidgetModel() {
    this.groupArray = new FormArray([]);
    for(var i = 0; i<this.widgetModelList.length; i++){
      this.groupArray.push(this.initializeFormGroupForArray(this.widgetModelList[i]))
    }
  }
  initializeGroupArrayFromData() {
    this.groupArray = new FormArray([]);
    for (var i = 0; i < this.data.length; i++) {
      let widgetModel = this.createWidgetModelForFormArray(this.data[i]);
      this.widgetModelList.push(widgetModel);
      this.groupArray.push(
        this.initializeFormGroupForArray(this.widgetModelList[i])
      );
    }
  }
  initializeEmptyFormArray() {
    this.groupArray = new FormArray([]);
    let widgetModel = this.createWidgetModelForFormArray(null);
    this.widgetModelList.push(widgetModel);
    this.groupArray.push(
      this.initializeFormGroupForArray(this.widgetModelList[0])
    );
  }
  initializeFormGroupForArray(model:WidgetModel) :FormGroup{
    return new FormGroup({});
  };
  createWidgetModelForFormArray(data:any):WidgetModel{
    return null;
  }
  associateArrayGroupToParent() {
    this.parent.setControl(this.WIDGET_GROUP_NAME,this.groupArray);
  }

  getFormattedValues(value:string,prefix:string,postfix:string,enforce=false):string{
    if(value && value !==null){
      if(enforce){
        return prefix+value+postfix;
      }
      else{
        if(value || value.length>0){
          return prefix+value+postfix;
        }
      }
      return'';
    }
    return'';
  }
  /**
   * takes care of unsubscribing to observers
   */
  ngOnDestroy(): void {
    this.unsubscribeSubscriber();
    this.cleanUp();
  }
  unsubscribeSubscriber(){
    for(var i = 0 ;i<this.subscriptionList.length;i++){
      this.subscriptionList[i].unsubscribe();
      this.subscriptionList[i] = null;
    }
  }
  addSubscriptionForReadMode(){
  }
  addSubscriptionForEditMode(){
  }

  cleanUp(){
    this.WIDGET_GROUP_NAME=undefined;
    this.compact=undefined;
    this.data=undefined;
    this.group=undefined;
    this.widgetModelList=undefined;
    this.widgetModel=undefined;
    this.subscriptionList=undefined;
    this.readOnly=undefined;
    this.parent=undefined;
    this.isWidgetArray=undefined;
    this.groupArray=undefined;
  }
}
