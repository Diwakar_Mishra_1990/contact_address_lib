import { TestBed } from '@angular/core/testing';

import { AddressPanelService } from './address-panel.service';

describe('AddressPanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddressPanelService = TestBed.get(AddressPanelService);
    expect(service).toBeTruthy();
  });
});
